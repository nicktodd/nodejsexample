const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const parser = require("body-parser");
const PORT = 4000;
const openingRoute = require("./routes/GetOpening");

let app = express();
app.use(cors());
app.use(parser.json());

app.use("/opening", openingRoute);

mongoose
  .connect("mongodb://localhost:27017/cinema")
  .then(() => console.log("successfully connected to the DB"))
  .catch(err => console.log("connection failed " + err));

const server = app.listen(PORT, () => {
  const SERVER_HOST = server.address().address;
  console.log("server at " + SERVER_HOST + ":" + PORT);
});
