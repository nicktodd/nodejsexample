const mongoose = require("mongoose");

const OpeningTimesSchema = mongoose.Schema({
  day: String,
  opening: String,
  closing: String
});

module.exports = mongoose.model("OpeningTimes", OpeningTimesSchema);
