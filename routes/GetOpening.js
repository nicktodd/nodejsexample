const express = require("express");
const router = express.Router();
const openingTimes = require("../models/OpeningTimes.model");
const parser = require("body-parser");

router.use(parser.json());

router.route("/").get((req, res) => {
  openingTimes.find((error, openingTimes) => {
    error ? res.status(404).send("not found") : res.json(openingTimes);
  });
});

module.exports = router;
